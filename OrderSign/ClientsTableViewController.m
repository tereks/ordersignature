//
//  ClientsTableViewController.m
//  OrderSign
//
//  Created by Sergey on 17.10.14.
//
//

#import "ClientsTableViewController.h"

#import "AFNetworking.h"
#import "UIView+Activity.h"
#import "OrderViewController.h"

@interface ClientsTableViewController ()

@property (strong) NSArray * clients;
@property (strong) NSString * clientId;
@property (strong) NSString * clientPhone;

@end

@implementation ClientsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = NO;
    
    [self loadClients];
}

- (void) loadClients {
    _clients = nil;
    [self.view startActivityIndicatorWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    NSDictionary * params = @{ @"username" : _login, @"pass" : _password };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager GET:@"http://getreport.ru/api/store/userlist/" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.view stopActivityIndicator];
        
        _clients = responseObject[@"clients"];
        
        [self.tableView reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.view stopActivityIndicator];
        
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                        message:@"Ошибка получения списка клиентов"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
    }];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_clients count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ClientCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.text = _clients[indexPath.row][@"name"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _clientId = _clients[indexPath.row][@"id"];
    _clientPhone = _clients[indexPath.row][@"phone"] != [NSNull null] ? _clients[indexPath.row][@"phone"] : nil;
    
    [self performSegueWithIdentifier:@"show_order" sender:self];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"show_order"]) {
        OrderViewController *vc = (OrderViewController*)[segue destinationViewController];
        vc.login = _login;
        vc.password = _password;
        vc.clientId = _clientId;
        vc.clientPhone = _clientPhone;
    }
}


@end
