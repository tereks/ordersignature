//
//  ViewController.h
//  OrderSign
//
//  Created by Sergey on 17.10.14.
//
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController

@property (strong) IBOutlet UITextField * login;
@property (strong) IBOutlet UITextField * password;

- (IBAction) loginSelected:(id)sender;

@end

