//
//  ClientsTableViewController.h
//  OrderSign
//
//  Created by Sergey on 17.10.14.
//
//

#import <UIKit/UIKit.h>

@interface ClientsTableViewController : UITableViewController

@property (strong) NSString* login;
@property (strong) NSString* password;

@end
