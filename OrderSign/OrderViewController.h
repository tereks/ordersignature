//
//  OrderViewController.h
//  OrderSign
//
//  Created by Sergey on 17.10.14.
//
//

#import <UIKit/UIKit.h>
#import "SignatureView.h"

@interface OrderViewController : UIViewController <UIAlertViewDelegate>

@property (nonatomic, strong) IBOutlet SignatureView *signatureView;
@property (nonatomic, strong) IBOutlet UITextField *amountField;
@property (nonatomic, strong) IBOutlet UITextField *commentField;

@property (nonatomic, strong) IBOutlet UITextField *phoneField;

@property (nonatomic, strong) IBOutlet UIButton *rublButton;
@property (nonatomic, strong) IBOutlet UIButton *buckButton;

@property (strong) NSString* login;
@property (strong) NSString* password;
@property (strong) NSString* clientId;
@property (strong) NSString* clientPhone;

- (IBAction) clearSelected:(id)sender;
- (IBAction) sendSelected:(id)sender;

- (IBAction) moneySelected:(id)sender;

@end
