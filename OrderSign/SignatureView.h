//
//  NICSignatureViewQuartzQuadratic.h
//
//  Created by Jason Harwig on 11/6/12.
//  Copyright (c) 2012 Near Infinity Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignatureView : UIView

@property (assign, readonly) BOOL hasSignature;

- (void) erase;

@end
