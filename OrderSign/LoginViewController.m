//
//  ViewController.m
//  OrderSign
//
//  Created by Sergey on 17.10.14.
//
//

#import "LoginViewController.h"
#import "AFNetworking.h"
#import "UIView+Activity.h"
#import "ClientsTableViewController.h"

@interface LoginViewController () <UITextFieldDelegate>

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.login becomeFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ( [textField isEqual:self.login] ) {
        [self.password becomeFirstResponder];
    }
    if ( [textField isEqual:self.password] ) {
        [self loginSelected:nil];
    }
    
    return YES;
}

- (IBAction) loginSelected:(id)sender {
    [self.view startActivityIndicatorWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    if ( _login.text.length == 0 || _password.text == 0 ) {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                        message:@"Пожалуйста, заполните все поля"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    [_password resignFirstResponder];
    [_login resignFirstResponder];
    
    NSDictionary * params = @{ @"username" : _login.text, @"pass" : _password.text };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager GET:@"http://getreport.ru/api/store/auth/" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.view stopActivityIndicator];
        
        if ( responseObject && [responseObject[@"status"] isEqualToString:@"err"] ) {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                            message:@"Пользователь с такими данными не найден"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            [alert show];
            return;
        }
        
        [self performSegueWithIdentifier:@"show_main" sender:self];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.view stopActivityIndicator];
        
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                        message:@"Пользователь с такими данными не найден"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
    }];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"show_main"]) {
        ClientsTableViewController *vc = (ClientsTableViewController*)[segue destinationViewController];
        vc.login = _login.text;
        vc.password = _password.text;
    }
}

@end
