//
//  OrderViewController.m
//  OrderSign
//
//  Created by Sergey on 17.10.14.
//
//

#import "OrderViewController.h"
#import "AFNetworking.h"
#import "UIView+Activity.h"

#import <QuartzCore/QuartzCore.h>

@interface OrderViewController () <UITextFieldDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) NSString *phoneNumberInputMask;
@property (strong, nonatomic) NSString *phonePass;

@end

@implementation OrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.phoneNumberInputMask = @"7 ### ###-##-##";

    self.phoneField.text = _clientPhone ? _clientPhone : @"7" ;
    [_amountField becomeFirstResponder];
}

- (IBAction) clearSelected:(id)sender {
    [_signatureView erase];
}

- (IBAction) moneySelected:(id)sender {
    if ( [sender isEqual:_rublButton] ) {
        _rublButton.enabled = NO;
        _buckButton.enabled = YES;
    }
    else {
        _rublButton.enabled = YES;
        _buckButton.enabled = NO;
    }
}

- (IBAction) sendSelected:(id)sender {
    if ( _signatureView.hasSignature ) {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Отправка"
                                                        message:@"Вы действительно хотите отправить данные?"
                                                       delegate:self
                                              cancelButtonTitle:@"Да"
                                              otherButtonTitles:@"Нет", nil];
        alert.tag = 21;
        [alert show];
    }
}

- (void) sendData {
    [self.view startActivityIndicator];
    [_amountField resignFirstResponder];
    
    UIImage * signatureImage = [self imageWithView:_signatureView];
    NSString *encodedImage = [UIImagePNGRepresentation(signatureImage) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSCharacterSet *nonPhoneSymbolSet = [NSCharacterSet characterSetWithCharactersInString: @"+()_- "];
    NSString * phonePass = [[self.phoneField.text componentsSeparatedByCharactersInSet: nonPhoneSymbolSet] componentsJoinedByString:@""] ;
    NSString * curr = @"";
    if ( !_rublButton.enabled ) {
        curr = @"usd";
    }
    else if ( !_buckButton.enabled ) {
        curr = @"rur";
    }
    
    NSDictionary *parameters = @{ @"username" : _login, @"pass" : _password, @"image" : encodedImage, @"client_id" : _clientId,
                                  @"comment" : _commentField.text, @"amount" : _amountField.text,
                                  @"phone" : phonePass, @"curr" : curr };
    
    [manager POST:@"http://getreport.ru/api/store/save/" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.view stopActivityIndicator];
        
        if ( [responseObject[@"saved"] isEqual:@(1)] ) {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Сохранение"
                                                            message:@"Данные сохранены успешно"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            alert.tag = 11;
            [alert show];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.view stopActivityIndicator];
        
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                        message:@"Ошибка сохранения"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
    }];
}

- (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0f);
    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:NO];
    UIImage * snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snapshotImage;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ( textField.tag == 10 ) {
        NSUInteger lengthOfString = string.length;
        for (NSInteger index = 0; index < lengthOfString; index++) {
            unichar character = [string characterAtIndex:index];
            if (character < 48) return NO; // 48 unichar for 0
            if (character > 57) return NO; // 57 unichar for 9
        }
        
        NSUInteger proposedNewLength = textField.text.length - range.length + string.length;
        if ( proposedNewLength > 6 )
            return YES;
    }
    else if ( textField.tag == 20 ) {
        NSString* newText = [NSString stringWithFormat:@"%@%@", textField.text, string];
        [textField setText:newText];
        
        NSString *changedString = textField.text;
        if ( [string isEqualToString:@""] )
        {
            NSInteger location = textField.text.length-1;
            if(location > 0)
            {
                for(; location > 0; location--)
                {
                    if(isdigit([changedString characterAtIndex:location]))
                    {
                        break;
                    }
                }
                changedString = [changedString substringToIndex:location];
            }
        }
        
        textField.text = [self filteredPhoneStringFromString:changedString withFilter:_phoneNumberInputMask];
        return NO;
    }
    
    return YES;
}

- (NSMutableString *)filteredPhoneStringFromString:(NSString *)string withFilter:(NSString *)filter {
    NSUInteger onOriginal = 0, onFilter = 0, onOutput = 0;
    char outputString[([filter length])];
    BOOL done = NO;
    
    while(onFilter < [filter length] && !done)
    {
        char filterChar = [filter characterAtIndex:onFilter];
        char originalChar = onOriginal >= string.length ? '\0' : [string characterAtIndex:onOriginal];
        switch (filterChar) {
            case '#':
                if(originalChar=='\0')
                {
                    // We have no more input numbers for the filter.  We're done.
                    done = YES;
                    break;
                }
                if(isdigit(originalChar))
                {
                    outputString[onOutput] = originalChar;
                    onOriginal++;
                    onFilter++;
                    onOutput++;
                }
                else
                {
                    onOriginal++;
                }
                break;
            default:
                outputString[onOutput] = filterChar;
                onOutput++;
                onFilter++;
                if(originalChar == filterChar)
                    onOriginal++;
                break;
        }
    }
    outputString[onOutput] = '\0'; // Cap the output string
    return [NSMutableString stringWithUTF8String:outputString];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if ( alertView.tag == 11 )
        [self.navigationController popViewControllerAnimated:YES];
    else if ( buttonIndex == 0 )
        [self sendData];
}

@end
