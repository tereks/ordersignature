//
//  AppDelegate.h
//  OrderSign
//
//  Created by Sergey on 17.10.14.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

